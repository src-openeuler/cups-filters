%global _service cups-browsed.service
Summary: OpenPrinting CUPS filters, backends, and cups-browsed
Name:    cups-filters
Version: 1.28.15
Release: 4
License: GPLv2 and GPLv2+ and GPLv3 and GPLv3+ and LGPLv2+ and MIT and BSD with advertising
Url:     http://www.linuxfoundation.org/collaborate/workgroups/openprinting/cups-filters
Source0: http://www.openprinting.org/download/cups-filters/cups-filters-%{version}.tar.xz

Patch6000: backport-CVE-2023-24805.patch
Patch6001: backport-CVE-2024-47175.patch
Patch6002: backport-CVE-2024-47076.patch
Patch6003: backport-CVE-2024-47176.patch
Patch6004: backport-CVE-2024-47850.patch

BuildRequires: pkgconf-pkg-config pkgconfig(libqpdf) pkgconfig(libpng) pkgconfig(dbus-1)
BuildRequires: poppler-cpp-devel libtiff-devel avahi-devel libjpeg-turbo-devel pkgconfig(zlib)
BuildRequires: pkgconfig(ijs) pkgconfig(freetype2) pkgconfig(lcms2) pkgconfig(poppler)
BuildRequires: pkgconfig(avahi-glib) pkgconfig(glib-2.0) pkgconfig(fontconfig) cups-ipptool
BuildRequires: systemd cups-devel python3-cups ghostscript gcc gcc-c++ poppler-utils
BuildRequires: autoconf automake dejavu-sans-fonts libtool gettext-devel

Requires: poppler-utils ghostscript cups-filesystem
Requires: liberation-mono-fonts bc grep sed which cups

%{?systemd_requires}

Obsoletes: ghostscript-cups
Obsoletes: foomatic-filters
Obsoletes: %{name}-libs < %{version}-%{release}
Provides: %{name}-libs = %{version}-%{release}

%description
This project provides backends, filters, and other software that was once
part of the core CUPS distribution but is no longer maintained by Apple
Inc. In addition it contains additional filters and software developed
independently of Apple, especially filters for the PDF-centric printing
workflow introduced by OpenPrinting and a daemon to browse Bonjour broadcasts
of remote CUPS printers to make these printers available locally and to
provide backward compatibility to the old CUPS broadcasting and browsing
of CUPS 1.5.x and older.

%package devel
Summary: Development files for %{name}
License: LGPLv2 and MIT
Requires: %{name}-libs = %{version}-%{release}

%description devel
This package contains the development files for %{name}

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
./autogen.sh

%configure --disable-static \
           --with-pdftops=hybrid \
           --enable-driverless \
           --with-rcdir=no \
           --enable-dbus \
           --disable-silent-rules \
           --disable-mutool \
           --enable-auto-setup-driverless

%make_build

%install
%make_install DESTDIR=%{buildroot}

install -d %{buildroot}%{_pkgdocdir}/fontembed/
cp -p fontembed/README %{buildroot}%{_pkgdocdir}/fontembed/
install -d  %{buildroot}%{_unitdir}
install -p -m 644 utils/%{_service} %{buildroot}%{_unitdir}
ln -sf %{_cups_serverbin}/filter/foomatic-rip %{buildroot}%{_bindir}/foomatic-rip

sed -i '/urftopdf/d' %{buildroot}%{_datadir}/cups/mime/cupsfilters.convs

%check
#make check

%post
%systemd_post %{_service}
/sbin/ldconfig

if [ $1 -eq 1 ] ; then
    IN=%{_sysconfdir}/cups/cupsd.conf
    OUT=%{_sysconfdir}/cups/cups-browsed.conf
    keyword=BrowsePoll
    if [ -f "$OUT" ]; then
        echo -e "\n# NOTE: This file is not part of CUPS.\n# You need to enable cups-browsed service\n# and allow ipp-client service in firewall." >> "$OUT"
    fi

    if [ -f "$IN" ] && grep -iq ^$keyword "$IN"; then
        if ! grep -iq ^$keyword "$OUT"; then
            (cat >> "$OUT" <<EOF
EOF
            ) || :
            (grep -i ^$keyword "$IN" >> "$OUT") || :
        fi
        sed -i -e "s,^$keyword,#$keyword directive moved to cups-browsed.conf\n#$keyword,i" "$IN" || :
    fi
fi

%preun
%systemd_preun %{_service}

%postun
%systemd_postun_with_restart %{_service}
/sbin/ldconfig


%files
%{_pkgdocdir}/AUTHORS
%{_pkgdocdir}/COPYING
%{_pkgdocdir}/fontembed/README
%config(noreplace) %{_sysconfdir}/cups/cups-browsed.conf
%attr(0755,root,root) %{_cups_serverbin}/filter/*
%attr(0755,root,root) %{_cups_serverbin}/backend/parallel
%attr(0700,root,root) %{_cups_serverbin}/backend/serial
%attr(0755,root,root) %{_cups_serverbin}/backend/implicitclass
%attr(0755,root,root) %{_cups_serverbin}/backend/beh
%attr(0755,root,root) %{_cups_serverbin}/backend/cups-brf
%{_bindir}/foomatic-rip
%{_bindir}/driverless
%{_bindir}/driverless-fax
%{_datadir}/cups/ppdc/*
%{_datadir}/cups/drv/*
%{_datadir}/cups/mime/*
%{_datadir}/cups/banners
%{_datadir}/cups/braille
%{_datadir}/cups/charsets
%{_datadir}/cups/data/*
%{_sbindir}/cups-browsed
%{_datadir}/ppd/cupsfilters
%dir %{_pkgdocdir}/
%{_libdir}/libcupsfilters.so.1*
%{_libdir}/libfontembed.so.1*
%{_unitdir}/%{_service}
%{_cups_serverbin}/backend/driverless
%{_cups_serverbin}/backend/driverless-fax
%{_cups_serverbin}/driver/driverless
%{_cups_serverbin}/driver/driverless-fax
%exclude %{_bindir}/ttfread
%exclude %{_libdir}/libcupsfilters.la
%exclude %{_libdir}/libfontembed.la

%files devel
%{_datadir}/cups/ppdc/escp.h
%{_includedir}/cupsfilters
%{_includedir}/fontembed
%{_libdir}/libcupsfilters.so
%{_libdir}/libfontembed.so
%{_libdir}/pkgconfig/libcupsfilters.pc
%{_libdir}/pkgconfig/libfontembed.pc

%files help
%{_pkgdocdir}/
%{_mandir}/man1/driverless.1.gz
%{_mandir}/man1/foomatic-rip.1.gz
%{_mandir}/man5/cups-browsed.conf.5.gz
%{_mandir}/man8/cups-browsed.8.gz

%changelog
* Wed Oct 9 2024 yanglu <yanglu72@h-partners.com> - 1.28.15-4
- fix CVE-2024-47850

* Sun Sep 29 2024 zhangxianting <zhangxianting@uniontech.com> - 1.28.15-3
- fix CVE-2024-47175 CVE-2024-47076 CVE-2024-47176

* Fri May 26 2023 zhouwenpei <zhouwenpei1@h-partners.com> - 1.28.15-2
- fix CVE-2023-24805

* Wed Aug 31 2022 tianlijing <tianlijing@kylinos.cn> - 1.28.15-1
- update to 1.28.15

* Sun Jan 10 2021 hanhui <hanhui15@huawei.com> - 1.28.9-2
- fix build err,can not find foomatic-rip

* Fri Jul 30 2021 yushaogui <yusahogui@huawei.com> - 1.28.9-1
- update cups-filtres package to 1.28.9

* Tue Jan 21 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.26.1-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: modify print err info when install

* Tue Nov 12 2019 caomeng<caomeng5@huawei.com> - 1.26.1-1
- Update version to 1.26.1

* Tue Nov 12 2019 caomeng<caomeng5@huawei.com> - 1.20.3-10
- Package init
